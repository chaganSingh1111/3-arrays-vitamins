const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 

    //Get all items that are available 

    const availableIems = items.filter((item)=> {
        return item.available=== true
    })

    //console.log(availableIems)

    //Get all items containing only Vitamin C.

    const availableVitC = items.filter((item)=> {
        return item.contains=== "Vitamin C"
    })

    //console.log(availableVitC)



    //Get all items containing Vitamin A.
    const allItemVitA = items.filter((item)=> {
        return item.contains.includes("Vitamin A")
    })

    //console.log(allItemVitA)


    //Group items based on the Vitamins that they contain

    const groupItems = items.map((item)=> {
        obj={}
        if(obj[item.contains.split(",").map((it2)=>{
            return it2
        })]===undefined){
            item.contains.split(",").map((it)=>{
                return obj[it] = item.name
            })
        }
        else{
            item.contains.split(",").map((it3)=>{
                obj[it3] = item.name
            })
        }
        return obj
    })
    console.log(groupItems)


    //sort on the base of Number of vitamin
    // const sortItem = items.sort((item1,item2)=>{
    //     let Item1 =  item1.contains.split(",").length
    //     let Item2 =  item2.contains.split(",").length
    //     return Item2 - Item1
    // })
    // console.log(sortItem)